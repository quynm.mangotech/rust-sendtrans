use std::env;
use std::str::FromStr;
use secp256k1::SecretKey;

use web3::contract::tokens::Tokenize;
use web3::contract::{Contract};
use web3::types::{TransactionParameters, Address, H160, U256, Bytes, H256, U64};

fn wei_to_eth(wei_val: U256) -> f64 {
    let res = wei_val.as_u128() as f64;
    res / 1_000_000_000_000_000_000.0
}

#[tokio::main]
async fn main() -> web3::Result<()> {
    dotenv::dotenv().ok();

    let websocket = web3::transports::WebSocket::new(&env::var("INFURA_RINKEBY").unwrap()).await?;
    let web3s = web3::Web3::new(websocket);

    let mut accounts = web3s.eth().accounts().await?;
    
    accounts.push(H160::from_str(&env::var("ACCOUNT_ADDRESS").unwrap()).unwrap());
    println!("Accounts: {:?}", accounts);

    for account in accounts {
        let balance = web3s.eth().balance(account, None).await?;
        println!(
            "Eth balance of {:?}: {}",
            account,
            wei_to_eth(balance)
        );
    }

    let prvk = SecretKey::from_str("438593fe85ef58ec50c09248b3fc32f19c9000cf1e6bf90f7ef9d9c706379a7e").unwrap();
    let to = Address::from_str("0x6C39383B80Edbb4e48bb2A388B47452bd69e8C41").unwrap();

    let token_contract =
        Contract::from_json(web3s.eth(), to, include_bytes!("erc20_abi.json")).unwrap();

    // let tx_data = token_contract.abi()
    //     .function("transferToken").unwrap()
    //     .encode_input(&(U256::one(), U256::exp10(17)).into_tokens()).unwrap();
    // let tx_object = TransactionParameters {
    //     to: Some(to),
    //     value: U256::exp10(17), //0.1 eth
    //     data: Bytes(tx_data.clone()),
    //     ..Default::default()
    // };

    let tx_data = token_contract.abi()
    .function("mintNFT").unwrap()
    .encode_input(
        &(
            Address::from_str("0xbaAe65FA1caCEDd9b24b35f053d8A28d8A50455c").unwrap(), 
            String::from("https://docs.rs/web3/latest/web3/signing/struct.SecretKeyRef.html")
        )
        .into_tokens()).unwrap();

    let tx_object = TransactionParameters {
        to: Some(to),
        data: Bytes(tx_data.clone()),
        gas: U256::from(1000_000u64),
        gas_price: Some(U256::exp10(9)),
        ..Default::default()
    };

    let signed = web3s.accounts().sign_transaction(tx_object, &prvk).await?;
    let result = web3s.eth().send_raw_transaction(signed.raw_transaction).await?;
    println!("{}", result);


    let receipt= web3s.eth().transaction_receipt(H256::from_str("0x3e604fcdd2a7cc29cdc5f9a05bc65f7da841b799a723a0a961a02bd4add7ad0b").unwrap()).await?;
    match receipt {
        Some(i) => {
            let status = i.status;
            if status == Some(U64::one()) {
                println!("Success");
            } else {
                println!("Failure");
            }
        },
        None => println!("Pending")
    }
    Ok(())

}
